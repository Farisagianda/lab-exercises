def main():
        redCircle = Circle(100, 100, 10, RedCircle())
        greenCircle = Circle(100, 100, 10, GreenCircle())
        redCircle.draw()
        greenCircle.draw()

class BridgePatternDemo(object):
    def main(args):
        redCircle = Circle(100, 100, 10, RedCircle())
        greenCircle = Circle(100, 100, 10, GreenCircle())
        redCircle.draw()
        greenCircle.draw()


class Circle(object):
    Shape = property()

    def __init__(self, x, y, radius, drawAPI):
        self.super(drawAPI)
        self._x = x
        self._y = y
        self._radius = radius

    def draw(self):
        drawAPI.drawCircle(self._radius, self._x, self._y)

class Shape(object):
    def __init__(self, drawAPI):
        self._drawAPI = drawAPI

    def draw(self):
        pass

class DrawAPI(object):
    pass

class RedCircle(object):
    DrawAPI = property()

    def drawCircle(self, radius, x, y):
        Console.WriteLine("Drawing Circle[ color: red, radius: " + radius + ", x: " + x + ", " + y + "]")

class GreenCircle(object):
    DrawAPI = property()

    def drawCircle(self, radius, x, y):
        Console.WriteLine("Drawing Circle[ color: green, radius: " + radius + ", x: " + x + ", " + y + "]")

main()
